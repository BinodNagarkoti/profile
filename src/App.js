import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";
import profile from "./profile.jpg";
// import profile from './assests/profile.jpg'
import { Container, ProgressBar, Card, Row, Col } from "react-bootstrap";
class App extends Component {
  state = {
    img_url: profile,
    name: "Binod Nagarkoti",
    contact: "9803235946",
    email: "Binod1365@gmail.com",
    address: "Boudha, Tinchuli",
    githubUsername: "binodnagarkoti",
    projects: [
      { name: "Counter", progress: 90 },
      { name: "Form", progress: 92 },
      { name: "TodoList", progress: 70 },
      { name: "API-Todo-List", progress: 80 }
    ]
  };
  render() {
    return (
      <Container fluid>
        <Row>
          <Col style={{ textAlign: "center" }}>
            <Card>
              <Card.Header>Profile</Card.Header>
              <Card.Img variant="top" src={this.state.img_url} />
              <Card.Body>
                GitLab Username:{" "}
                <Card.Link href="https://gitlab.com/BinodNagarkoti">
                  {this.state.githubUsername}
                </Card.Link>
              </Card.Body>
            </Card>
          </Col>
          <Col sm={3} style={{ textAlign: "left" }}>
            <Card>
              <Card.Header>Details</Card.Header>
              <Card.Body>
                <Card.Text>Name : {this.state.name}</Card.Text>
                <Card.Text>Contact : {this.state.contact}</Card.Text>
                <Card.Text>Email : {this.state.email}</Card.Text>
                <Card.Text>Address : {this.state.address}</Card.Text>
                <Card.Text>Name : {this.state.name}</Card.Text>
              </Card.Body>
            </Card>
          </Col>
          <Col>
            <Card>
              {" "}
              <Card.Header>Projects</Card.Header>
              <Card.Body>
                {this.state.projects.map(value => (
                  <Card.Text>
                    {value.name}
                    <ProgressBar
                      variant="success"
                      label={value.progress + "%"}
                      now={value.progress}
                    />
                  </Card.Text>
                ))}
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default App;
